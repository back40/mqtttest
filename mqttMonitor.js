var mqtt = require('mqtt');		// https:www.npmjs.com/package/mqtt
//var Broker_URL = 'mqtt://3.16.194.135';
//var Broker_URL = 'tcp://3.16.194.135';
var Broker_URL = 'tcp://localhost';
//var Topic = '#'; //subscribe to all topics
var TopicMsg = 'volvo/vehicleMsgs/msg';
var TopicAck = 'volvo/vehicleMsgs/ack';

var connectOptions = {
	clientId: 'MyMQTT',
	port: 1883,
	keepalive : 60
};

var client = mqtt.connect(Broker_URL, connectOptions);
client.on('connect', mqtt_connect);
client.on('reconnect', mqtt_reconnect);
client.on('error', mqtt_error);
client.on('message', mqtt_messsageReceived);
client.on('close', mqtt_close);

function mqtt_connect() {
    console.log("Connecting MQTT");
    client.subscribe(TopicMsg, mqtt_subscribe);
}

function mqtt_subscribe(err, granted) {
    console.log("Subscribed to " + TopicMsg);
    if (err) {console.log(err);}
}

function mqtt_reconnect(err) {
    console.log("Reconnect MQTT");
    if (err) {console.log(err);}
	client  = mqtt.connect(Broker_URL, options);
}

function mqtt_error(err) {
    console.log("Error!");
	if (err) {console.log(err);}
}

function after_publish() {
	//do nothing
}

function mqtt_messsageReceived(topic, message, packet) {
	console.log('\nReceived: Topic = ' +  topic + '  Message = ' + message);

	if (topic == TopicMsg) {
		/* Received format:
			[{
				"msgId":"123","deviceId":"456","userId":"333","content":"Some msg!"
		    },{
				"msgId":"5675","deviceId":"6677","userId":"975","content":"Some msg!"
		    }]

			[{"msgId":"123","deviceId":"456","userId":"333","content":"Some msg!"},{"msgId":"5675","deviceId":"6677""userId":"975","content":"Some msg!"}]

		  Returned format:
		  {"ack:":["123","5675"]}
		*/

		var ackMsg = {};
		var key = 'ids';
		ackMsg[key] = [];

		var jsonParsed = JSON.parse(message)
		console.log('Length = ' + jsonParsed.msgs.length);

		// console.log('inner');
		for(var attributename in jsonParsed.msgs){
			ackMsg[key].push(jsonParsed.msgs[attributename].id);
		}

		client.publish(TopicAck, JSON.stringify(ackMsg));
		console.log('Reply: Topic = ' +  TopicAck + '  Message = ' + JSON.stringify(ackMsg));
	}
}

function mqtt_close() {
	console.log("Close MQTT");
}
